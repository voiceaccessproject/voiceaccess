//import java.io.IOException;

public class LCD {
	
	public static void print1(String content)
	{
		System.out.print(content+"\n");
	}

	public static void print(String content) {
		//
		/*try {
			Runtime.getRuntime().exec("CLS");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		String firstLine;
		String secondLine;
		/*
		 * for (int i=0; i<content.length(); i++) { firstLine =
		 * content.substring(0, 8); }
		 */
		if (content.length() > 8)
			firstLine = content.substring(0, 8);
		else {
			firstLine = content;
			System.out.print(firstLine+"\n\n");
			return;
		}
		secondLine = content.substring(8, content.length());
		if(secondLine.charAt(0) == ' ') secondLine = secondLine.substring(1, secondLine.length());
		String endOfFirstLine = firstLine.split(" ")[firstLine.split(" ").length - 1];
		if (endOfFirstLine.length() < 2) {
			//
			secondLine = endOfFirstLine + secondLine;
			StringBuffer fLine = new StringBuffer(firstLine);
			fLine.replace(firstLine.lastIndexOf(" "), firstLine.length(), "");
			firstLine = fLine.toString();
		}

		if (secondLine.length() > 8)
			secondLine = secondLine.substring(0, 8);

		System.out.print(firstLine + "\n" + secondLine+"\n\n");
	}

}
