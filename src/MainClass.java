import marf.MARF;
import marf.util.Debug;
import marf.util.MARFException;

public class MainClass {
	
	public static final void setDefaultConfig()
			throws MARFException
			{
				/*
				 * Default MARF setup
				 */
				MARF.setPreprocessingMethod(MARF.DUMMY);
				MARF.setFeatureExtractionMethod(MARF.FFT);
				MARF.setClassificationMethod(MARF.EUCLIDEAN_DISTANCE);
				MARF.setDumpSpectrogram(false);
				MARF.setSampleFormat(MARF.WAV);

				Debug.enableDebug(false);
			}
	
	static int recognizeVoice()
	{
		//
		MARF.setSampleFile("tmp.wav");
		try {
			MARF.recognize();
		} catch (MARFException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return MARF.queryResultID();
	}
	
	public static void addNewUser(UsersDB users) throws Exception
	{
		//
		LCD.print1("Name:");
		String name = new String();
		name = System.console().readLine();
		String userPin = new String();
		users.recordSample();
		userPin = pinRecogn.recognize("tmp.wav");
		System.out.println("Pin of "+name+" is "+userPin);
		System.out.println("Done!");
		int userId = users.addUser(name, userPin);
		
		MARF.setSampleFile("tmp.wav");
		MARF.setCurrentSubject(userId);
		try {
			MARF.train();
		} catch (MARFException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
	}
	
	public static void usingMode(UsersDB users)
	{
		LCD.print("Say something");
	}
	
	public static void main(String [] args)
	{
		//System.out.println("Hello!");

		UsersDB users = new UsersDB("users.db");
		try {
			setDefaultConfig();
		} catch (MARFException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		for(;;)
		{
			users.recordSample();
			try {
				String key = pinRecogn.recognize("tmp.wav");
				System.out.println("[DEBUG]: "+key);
				if(key.equals(new String("1735")))
				{
					try {
						addNewUser(users);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					continue;
				}
				UserEntry foundUser= users.findByUserId(recognizeVoice());
				if(key.equals(foundUser.pin))
				{
					System.out.println("Access granted to user: "+foundUser.name+" ID: "+foundUser.id);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
		
		}
	}

}
