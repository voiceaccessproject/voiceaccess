
/*final class UsersEntry {
	String name;
	String password;
	void UserEntry()
}*/
//import java.io.File;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class UsersDB {
	
	List<UserEntry> db = new ArrayList<UserEntry>();
	String dbFilePath = new String();
	
	UsersDB(String filePath)
	{
		//
		dbFilePath = filePath;
		FileInputStream dbFile;
		String entryString;
		
		try {
			dbFile = new FileInputStream(filePath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		Scanner newLineScanner = new Scanner(dbFile);
		while(newLineScanner.hasNextLine())
		{
			entryString = newLineScanner.nextLine();
			String[] entryContent = entryString.split(" ");
			db.add(new UserEntry(entryContent[0], Integer.parseInt(entryContent[1]), entryContent[2]));
		}
		newLineScanner.close();
		
		try {
			dbFile.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void recordSample()
	{
		final JavaSoundRecorder recorder = new JavaSoundRecorder();
		 
        // creates a new thread that waits for a specified
        // of time before stopping
        Thread stopper = new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
                recorder.finish();
            }
        });
 
        stopper.start();
 
        // start recording
        recorder.start();
        //System.out.println("Rec");
	}
	
	int addUser(String userName, String userPin)
	{
		int userId = db.size()+1;
		db.add(new UserEntry(userName, userId, userPin));
		PrintWriter dbFile;
		try {
			dbFile = new PrintWriter(new FileOutputStream(new File(dbFilePath), true));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
		
		dbFile.println(userName+" "+Integer.toString(userId)+" "+userPin);
		dbFile.close();
		return userId;
	}
	
	UserEntry findByUserId(int userId)
	{
		//
		for(int i=0; i<db.size(); i++)
		{
			if(db.get(i).id == userId) return db.get(i);
		}
		return null;
	}

}
