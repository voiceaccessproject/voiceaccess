import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javaFlacEncoder.FLAC_FileEncoder;


public class pinRecogn {
	
	public static String recognize(String path) throws Exception
	{
		FLAC_FileEncoder flacEncoder = new FLAC_FileEncoder();
		File inFile = new File(path);
		File outFile = new File("tmp.flac");
		flacEncoder.encode(inFile, outFile);
		
		Path pathToFlac = new File("tmp.flac").toPath();
		byte[] data = Files.readAllBytes(pathToFlac);
		String request = "https://www.google.com/"+
                "speech-api/v1/recognize?"+
                "xjerr=1&client=speech2text&lang=en-US&maxresults=1";
		
		URL url = new URL(request);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setInstanceFollowRedirects(false);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "audio/x-flac; rate=8000");
		
		connection.setConnectTimeout(60000);
		connection.setUseCaches(false);
		DataOutputStream wr = new DataOutputStream(connection.getOutputStream());	
		wr.write(data);
		
		//wr.flush();
		//wr.close();
		//connection.disconnect();

		System.out.println("Done");

		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				
		String decodedString;
		String jsonResult = "";
		while ((decodedString = in.readLine()) != null) {
			//System.out.println(decodedString);
			jsonResult = decodedString;
		}
		
		//System.out.println("[DEBUG]: "+jsonResult);
		
		JSONParser parser = new JSONParser();
		Object obj;
		/*try {*/
			obj = parser.parse(jsonResult);
		/*} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}*/
		JSONObject jsonObj = (JSONObject) obj;
		JSONArray hypArray = (JSONArray)jsonObj.get("hypotheses");
		JSONObject targetObj = (JSONObject)hypArray.get(0);
		jsonResult="";
		return (String)targetObj.get("utterance");
	}

}
